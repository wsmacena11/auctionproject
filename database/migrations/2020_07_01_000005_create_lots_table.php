<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lots', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('book_id');
            $table->string('description','10000');
            $table->dateTime('start');
            $table->dateTime('end');
            $table->decimal('initial_value');
            $table->unsignedInteger('status_id');
            $table->unsignedInteger('image_id');
            $table->timestamps();
        });

        Schema::table('lots', function (Blueprint $table) {
            $table->foreign('book_id')->references('id')->on('books');
            $table->foreign('image_id')->references('id')->on('images');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lots');
    }
}

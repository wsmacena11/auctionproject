<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses' => 'HomeController@index', 'as' => 'home.index']);

// Route::get('/', function(){
//     return view('home');
// });



Auth::routes();

Route::get('book/index',['uses'=>'BookController@index', 'as' => 'book.index']);
Route::get('book/detail/{id}',['uses'=>'BookController@detail', 'as' => 'book.detail']);
Route::get('book/register',['uses'=>'BookController@register', 'as' => 'book.register']);

Route::post('/book/store', ['uses'=>'BookController@store', 'as' => 'book.store']);

Route::get('/book/edit/{id}', ['uses'=>'BookController@edit', 'as' => 'book.edit']);
Route::post('/book/update/{id}/{opt?}', ['uses'=>'BookController@update', 'as' => 'book.update']);
Route::post('/book/delete/{id}', ['uses'=>'BookController@delete', 'as' => 'book.delete']);




Route::post('/debug','StatusController@debug')->name('debug');

Route::post('/category/create',['uses'=>'CategoryController@store', 'as' => 'category.create']);

Route::get('/category/{id}', 'CategoryController@show');

Route::get('meuslivros/{id}', 'BookController@index2');

Route::get('status/index', 'StatusController@index')->name('status.index');
Route::get('panel/index', 'HomeController@panel')->name('panel.index');

// Route::post('/lote/create',['uses'=>'TagController@store', 'as' => 'lote.create']);
Route::post('/lot/store', 'LotController@store')->name('lot.store');

// Route::get('testes/index', 'HomeController@testes')->name('testes.index');

Route::get('lot/index', 'LotController@index')->name('lot.index');

Route::get('lot/bid/{id}', 'LotController@show')->name('lot.bid');
Route::post('/lot/bid/store', 'BidController@store')->name('lot.bid.store');

Route::get('testes/index', 'HomeController@testes')->name('testes.index');

<?php

namespace App;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class Lot extends Model
{
    protected $fillable = ['book_id, status_id, description, initial_value, created_at, updated_at'];
    protected $dates = ['start', 'end'];

    
}

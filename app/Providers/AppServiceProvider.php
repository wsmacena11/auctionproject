<?php

namespace App\Providers;

//use Dotenv\Validator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;



class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend(
            'validaBid', 
            'App\Libraries\Validations@validaBid',
            'Valor de bid precisa ser maior que o último bid!'
        );        
    }

}

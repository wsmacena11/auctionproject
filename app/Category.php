<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['nome'];

    public function book()
    {
        return $this->hasOne(Book::class, 'category_id', 'id');
    }
}

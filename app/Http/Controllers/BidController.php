<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Bid;
use Illuminate\Support\Facades\Log;
use Validator;

class BidController extends Controller
{
    public function store(Request $request){
        $bid = new Bid();
        $bid->lot_id         = $request->lot_id;
        $bid->user_id      = $request->user_id;
        $bid->value           = str_replace(',', '.', str_replace('.', '', $request->value));
        $bid->save();
        return redirect()->route('lot.bid', $bid->lot_id)->with('alert-success', 'Product updated successfully!');
    }
}

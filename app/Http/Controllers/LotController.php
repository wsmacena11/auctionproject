<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;
use App\Lot;
use App\Image;


class LotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lotbook = DB::table('lots')
        ->join('books', 'books.id', '=', 'lots.book_id')
        ->join('images', 'images.id', '=', 'lots.image_id')
        ->select('lots.id as lot', 'books.title', 'lots.start', 'lots.end', 'lots.description', 'lots.image_id', 'images.image')
        ->distinct()
        ->paginate(30);

        return view('lot.index', compact('lotbook'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //imagem e lot
        //teste 2
        $imagem = new Image();

        if ($request->hasFile('image')) {
           
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('img/lots', $filename);
            $imagem->image = $filename;
        }else{
            return $request;
            $imagem->image = '';
        }

        if ($imagem->save()) {

            $lot = new Lot();
            $lot->book_id           = $request->book_id;
            $lot->status_id         = $request->status_id;
            $lot->description       = $request->description;
            $lot->initial_value     = str_replace(',', '.', str_replace('.', '', $request->initial_value));
            $lot->start             = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y/m/d');
            $lot->end               = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y/m/d');
            $lot->image_id          = $imagem->id;
            $lot->save();
            return redirect()->route('panel.index')->with('alert-success', 'Product updated successfully!');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $lots = Lot::find(2);
        // dd($lots);
        $lotbook = DB::table('lots')
        ->join('books', 'books.id', '=', 'lots.book_id')
        ->join('images', 'images.id', '=', 'lots.image_id')
        ->leftJoin('bids', 'bids.lot_id','=','lots.id')
        ->select(
            'lots.id as lot',
            'books.title',
            'lots.initial_value',
            'lots.start',
            'lots.end',
            'lots.description',
            'lots.image_id',
            'images.image',
            DB::raw('max(bids.value) as final_value'))
        ->where('lots.id', '=', $id)
        ->groupBy('lots.id')
        ->get();

        $bids = DB::table('bids')
        ->join('users', 'users.id', '=', 'bids.user_id')
        ->select(
            'bids.value',
            'bids.updated_at as bid_date',
            'users.firstname as user_name'
        )
        ->where('bids.lot_id','=', $id)
        ->orderBy('bids.id', 'asc')
        ->get();

        //dd($lotbook);
        return view('lot.bid', compact('lotbook', 'bids'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

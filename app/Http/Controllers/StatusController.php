<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StatusController extends Controller
{
    public function index(){
        return view('status.index');
    }

    public function debug(Request $request){
        //var_dump($request->except(['_token']));
        $status = new \App\Status;
        $status->create($request->except(['_token']));


        return redirect()->route('status.index')->with('status','Status cadastrado!');
    }
}
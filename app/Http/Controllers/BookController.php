<?php

namespace App\Http\Controllers;

use App\Book;
use App\Category;

use Illuminate\Http\Request;

class BookController extends Controller
{
    public function __construct(){
    	$this->middleware('auth');
    }

    public function index(){
        $book = Book::where('inactive', '=', '0')->paginate(10);
        return view('book.index', compact('book'));
    }

    public function index2($request){
        // Para fins de teste
        $books = Book::where('category_id', $request)->first();
        $lid = $books->id;
        $categorylid = Category::where('id', $lid)->first();

        return view('book.books', compact('books', 'categorylid', 'request'));

    }

    public function detail($id){
        $book = Book::find($id);
        $category = \App\Category::find($book->category_id);

        return view('book.detail',compact('book', 'category'));
    }

    public function register(){
        return view('book.register');
    }

    public function store(Request $request){
        $book = new Book();
        $book->title                = $request->title;
        $book->subtitle             = $request->subtitle;
        $book->author                = $request->author;
        $book->publishing_company   = $request->publishing_company;
        $book->category_id          = $request->category_id;
        $book->inactive             = 0;
        $book->save();

        return redirect()->route('book.index', $book->id)->with('alert-success', 'Product updated successfully!');
    }

    public function edit($id)
    {
        $book = Book::findOrFail($id);
        $categorybook = Category::find($book->category_id);
        $categories = Category::all();
        return view('book.edit',compact('book', 'categorybook', 'categories'));
    }

    public function update(Request $request, $id){
        $book = \App\Book::find($id);
        $book->title                = $request->title;
        $book->subtitle             = $request->subtitle;
        $book->author               = $request->author;
        $book->publishing_company   = $request->publishing_company;
        $book->category_id          = $request->category_id;
        $book->update();

        return redirect()->route('book.detail', $id);

        // if(Book::find($id)->update($request->all())){
        //     \Session::flash('flash_message',[
        //         'msg'=>"Book updated successful!",
        //         'class'=>"alert-success retornomsg"
        //     ]);
        //     dd("testeupdate");
        //     // return redirect()->route('book.detail', $id);
        // }
        
    }

    public function delete(Request $request, $id)
    {   
        $book = \App\Book::find($id);
        $book->inactive = 1;
        $book->update();
        return redirect()->route('book.index', $id)->with('alert-success','Product hasbeen inactivated!');
    }
}

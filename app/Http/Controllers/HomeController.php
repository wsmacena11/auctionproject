<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Lot;
use App\Status;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $lotbook = DB::table('lots')
        ->join('books', 'books.id', '=', 'lots.book_id')
        ->join('images', 'images.id','lots.image_id')
        ->select('lots.id as lot', 'books.title', 'lots.start', 
            'lots.end', 'images.image', 'lots.description')
        ->where('lots.status_id', '=', '1')
        // ->distinct()
        ->orderByDesc('lots.end')
        ->take(3)
        ->get();
        return view('home', compact('lotbook'));
        
    }
    public function testes()
    {

        return view('testes.index');
    }

    public function panel(){
        $book = DB::table('books')->where('inactive','=','0')->get();
        $lot = Lot::all();
        $status = Status::all();

        $lotbook = DB::table('lots')
        ->join('books', 'books.id', '=', 'lots.book_id')
        ->select('lots.id as lot', 'books.title', 'lots.start', 'lots.end')
        ->distinct()
        ->get();
        return view('panel.index', compact('book','status' ,'lot', 'lotbook'));
    }
}

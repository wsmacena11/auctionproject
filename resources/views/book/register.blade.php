@extends('layouts.app')

@include('layouts._includes.nav')

@section('content')
	<div class="container">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
				<li class="breadcrumb-item"><a href="{{ url('/book/index') }}">Books</a></li>
				<li class="breadcrumb-item active" aria-current="page">Register</li>
			</ol>
		</nav>

		<div class="form-cadastro-livro">
			<form action="{{ route('book.store') }}" method="POST">
				@csrf
				<div class="form-row">
					<div class="col-3">
						<label for="title">Title</label>
						<input name="title" type="text" id="title" class="form-control" placeholder="">
					</div>

					<div class="col-3">
						<label for="subtitle">Sub-title</label>
						<input name="subtitle" type="text" id="subtitle" class="form-control" placeholder="">
					</div>

					<div class="col-4">
						<label for="author">Author</label>
						<input name="author" type="text" id="author" class="form-control font-italic" placeholder="">
					</div>

				</div>

				<div class="form-row">
					<div class="col-3">
						<label for="publishing_company">Publishing Company</label>
						<input name="publishing_company" type="text" id="publishing_company" class="form-control" placeholder="">
					</div>

					<div class="col-2">
						<label for="category_id">Category</label>
						<select name= "category_id" class="form-control" id="category_id">
							<option value="0" selected disabled></option>
							@foreach (App\Category::get() as $category)
							<option value='{{ $category->id }}'>{{ $category->name }}</option>
							@endforeach
						</select>
					</div>


				</div>
				<div class="form-row">

					<div class="col-3">
						<br>
						<button type="submit" class="btn btn-success">Add</button>
					</div>
				</div>

			</form>
		</div>






	</div>
@endsection

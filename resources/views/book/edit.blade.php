@extends('layouts.app')

@include('layouts._includes.nav')

@section('content')
	<div class="container">
        <nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
				<li class="breadcrumb-item"><a href="{{ route('book.index') }}">Books</a></li>
				<li class="breadcrumb-item"><a href="{{ route('book.detail',$book->id) }}">Details</a></li>
				<li class="breadcrumb-item active" aria-current="page">Edit</li>
			</ol>
		</nav>

		<div class="form-cadastro-livro">
			<form action="{{ route('book.update', $book->id) }}" method="POST">
				@csrf
				<div class="form-row">
					<div class="col-3">
						<label for="title">Title</label>
						<input  name="title" type="text" id="title" class="form-control" placeholder="" value="{{ $book->title }}">
					</div>

					<div class="col-3">
						<label for="subtitle">Subtitle</label>
						<input  name="subtitle" type="text" id="subtitle" class="form-control" placeholder="" value="{{ $book->subtitle }}">
					</div>

					<div class="col-4">
						<label for="author">Author</label>
						<input  name="author" type="text" id="author" class="form-control font-italic" placeholder="" value="{{ $book->author }}">
					</div>
				</div>

				<div class="form-row">
					<div class="col-3">
						<label for="publishing_company">Publishing Company</label>
						<input  name="publishing_company" type="text" id="publishing_company" class="form-control" placeholder="" value="{{ $book->publishing_company }}">
					</div>

					<div class="col-2">
						<label for="category_id">Category</label>
						<select name= "category_id" class="form-control" id="category_id">
							<option value="{{ $book->category_id }}">{{ $categorybook->name }}</option>
							@foreach ($categories as $categoryopt)
								@if ($categoryopt->id != $book->category_id)
								<option value="{{ $categoryopt->id }}">{{ $categoryopt->name }}</option>
								@endif
							@endforeach
						</select>
					</div>
				</div>

				<br>
				<div class="form-row">
					<div class="form-group float-left" ali>
                        <button type="submit" class="btn btn-success " ><i class="far fa-save fa-2x"></i></button>
                    </div>

                    


                            {{-- <div class="col-md-4 offset-md-11 col-xs-2 offset-xs-8 col-5 offset-11 col-lg-4 offset-lg-11">
                            </div> --}}
                </div>
			</form>
			
			<form action="{{ route('book.delete', $book->id) }}" method="POST">
				@csrf
				<div class="form-group float-right">
					<button class="btn btn-danger" > <a onclick="return confirm('Are you sure?')"></a> <i class="fas fa-trash-alt fa-2x " style="color: white"></i></button>
				</div>
			</form>

		</div>


	</div>
@endsection

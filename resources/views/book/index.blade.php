@extends('layouts.app')

@include('layouts._includes.nav')

@section('content')
<div class="container ">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Books</li>
        </ol>
    </nav>

    <p>
        <a class="btn btn-success"href="{{ route('book.register') }}">Add</a>
    </p>

        <table class="table table-striped   col-12">
            <thead>
                <tr class="d-flex">
                    <th class="col-1">#</th>
                    <th class="col-3">Title</th>
                    <th class="col-2">Author</th>
                    <th class="col-2">Publishing Company</th>
                    <th class="col-2">Category</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($book as $bookx)
                <tr class="d-flex">
                    <td class="col-1">{{ $bookx->id }}</td>
                    <td class="col-3">{{ $bookx->title }}</td>
                    <td class="col-2">{{ $bookx->author }}</td>
                    <td class="col-2">{{ $bookx->publishing_company }}</td>
                    <td class="col-4">{{ App\Category::find($bookx->category_id)->name }}</td>
                    <td class="">
                        <div class="row ">
                            <div class="col-2">
                                <a href="{{ route('book.detail', $bookx->id) }}"><i class="fas fa-search"></i></a>
                            </div>
                        </div>
                    </td>

                </tr>
                @endforeach

            </tbody>
        </table>


        {{ $book->links() }}

    </div>

</div>
@endsection

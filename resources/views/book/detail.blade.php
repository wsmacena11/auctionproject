@extends('layouts.app')

@include('layouts._includes.nav')

@section('content')
	<div class="container">
        <nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
				<li class="breadcrumb-item"><a href="{{ url('/book/index') }}">Books</a></li>
				<li class="breadcrumb-item active" aria-current="page">Details</li>
			</ol>
		</nav>

		<div class="form-cadastro-livro">
			<form >
				@csrf
				<div class="form-row">
					<div class="col-3">
						<label for="title">Title</label>
						<input readonly name="title" type="text" id="title" class="form-control" placeholder="" value="{{ $book->title }}">
					</div>

					<div class="col-3">
						<label for="subtitle">Subtitle</label>
						<input readonly name="subtitle" type="text" id="subtitle" class="form-control" placeholder="" value="{{ $book->subtitle }}">
					</div>

					<div class="col-4">
						<label for="author">Author</label>
						<input readonly name="author" type="text" id="author" class="form-control font-italic" placeholder="" value="{{ $book->author }}">
					</div>
				</div>

				<div class="form-row">
					<div class="col-3">
						<label for="publishing_company">Publishing Company</label>
						<input readonly name="publishing_company" type="text" id="publishing_company" class="form-control" placeholder="" value="{{ $book->publishing_company }}">
					</div>

					<div class="col-4">
						<label for="category_id">Category</label>
						<input readonly name="category_id" type="text" id="category_id" class="form-control" placeholder=""
							value="{{$category->name}}">
					</div>
				</div>
				<br>
            </form>
            <div class="form-row">
                <div class="form-group col-md-11">
                    <a class="btn btn-primary" href="{{ route('book.edit', $book->id) }}"> <i class="fas fa-pencil-alt fa-2x" style="color:white"></i> </a>
                </div>
            </div>

		</div>
	</div>
@endsection

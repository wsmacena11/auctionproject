<script>
    var target_date = new Date("{{ $query->end }}").getTime();
    var dias, horas, minutos, segundos;
    var regressiva = document.getElementById("regressiva");

    intervalo = setInterval(function () {
        var current_date = new Date().getTime();
        var segundos_f = (target_date - current_date) / 1000;
        if (segundos_f > 0) {
            
            dias = parseInt(segundos_f / 86400);
            segundos_f = segundos_f % 86400;
            
            horas = parseInt(segundos_f / 3600);
            segundos_f = segundos_f % 3600;
            
            minutos = parseInt(segundos_f / 60);
            
            segundos = parseInt(segundos_f % 60);
            
            if (horas < 10) {
                horas = "0" + horas;
            }
            if (minutos < 10) {
                minutos = "0" + minutos;
            }
            if (segundos < 10) {
                segundos = "0" + segundos;
            }
            
            document.getElementById('dia').innerHTML = dias+"d ";
            document.getElementById('hora').innerHTML =  horas+":";
            document.getElementById('minuto').innerHTML = minutos+":";
            document.getElementById('segundo').innerHTML = segundos;
        }
    }, 1000);
</script>
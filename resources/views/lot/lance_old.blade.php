@extends('layouts.app')

@include('layouts._includes.nav')

@section('content')
<div class="container">
    @foreach ($lotelivro as $query)
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ url('/lote/index') }}">Lotes</a></li>
            <li class="breadcrumb-item active" aria-current="page">Detalhe - Lote nº: {{ $query->lote }}</li>
        </ol>    
    </nav>
    @if (($query->fim) < date('Y-m-d H:i:s') )
        <h1>Encerrado!</h1>
    @else
    <div>
        <h2>{{ $query->titulo_livro }}</h2>
        <hr>
        <div id="lance" style="position: relative"  class="lance-imagem">
            <img style="border-radius: 10px" class="responsive center" src="{{ asset('img/lotes/' . $query->image) }}" alt="Card image cap">
    
            <br>
            <h4>Informações</h4>
            <div class="info-lote">
                <div>Lote: <span>{{ $query->lote }}</span></div>
                <div>Valor Inicial: <span>R${{ $query->valor_inicial }}</span></div>
                <div>Data Início: <span>{{ $query->dia_inicio }}</span></div>
                <div>Data Encerramento: <span>{{ $query->fim }}</span></div>
            </div>

            <div id="lance">
                <h4>Descrição Lote</h4>
                <div class="descricao-lote">
                    {{ $query->descricao_lote }}
                </div>
            </div>
        </div>
        
        <br>
        <div id="lance">
            <div class="lance" style="background-color: #eeeeee">
                <div style="font: italic; background-color:rgba(6, 33, 92, 0.842); color: white" align="center">Aberto para lance</div>
                <div align="center">Ultimo Lance</div>
                <div align="center"><span style="font-size:x-large ">R${{ $query->valor_inicial }}</span></div>
            </div>
            <button class="botao-lance">Dar Lance</button>
        </div>
        
        <br>
        <div id="lance" style="float: inline-end">
            <div class="contagem">
                <div>
                    <span class="numero" id="dia"></span><span class="numero" id="hora"></span><span class="numero" id="minuto"></span><span class="numero" id="segundo"></span> 
                </div>
            </div>
        </div>

        <br>
    </div>
    
    @endif
    @endforeach
</div>

@extends('lote.functions.app', ['query' => $query])

@endsection
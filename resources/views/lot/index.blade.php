@extends('layouts.app')

@include('layouts._includes.nav')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item active"><a>Lots</a></li>
            
        </ol>    
    </nav>

    <?php
        $produto = 50;
        $saldo = 8;
        $result = ($saldo * 100) / $produto;

        if($result < 10){
            echo "Estoque menor que 10%";
        }

    ?>
    <h1 style="text-align: center">Lotes Cadastrados</h1>
    <br>
    @foreach ($lotbook->chunk(3) as $lots)
        <div class="row">
            @foreach ($lots as $query)
            <div class="col-sm-4">
                <div class="card">
                    <img class="card-img-top" src="{{ asset('img/lots/' . $query->image) }}" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">Lote Nº: {{ $query->lot }}</h5>
                        <p class=" card-text-right">{{ $query->description }}</p>
                        
                        <div>
                            <a href="{{ route('lot.bid', $query->lot) }}" class="btn btn-primary">Detalhes</a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <br>
    @endforeach
</div>
@endsection
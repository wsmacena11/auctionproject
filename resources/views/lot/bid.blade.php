@extends('layouts.app')

@include('layouts._includes.nav')

@section('content')
<div class="container">
    @foreach ($lotbook as $query)
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ url('/lote/index') }}">Lots</a></li>
            <li class="breadcrumb-item active" aria-current="page">Details - Lot nº: {{ $query->lot }}</li>
        </ol>    
    </nav>
    @if (($query->end) < date('Y-m-d H:i:s') )
        <h1>Closed!</h1>
    @else
    <div>
        <h2>{{ $query->title }}</h2>
        <hr>

        <div class="row">
            <div class="col-sm">
                <img style="border-radius: 10px" class="responsive center" src="{{ asset('img/lots/' . $query->image) }}" alt="Card image cap">
                <br>
                <h4>Informações</h4>
                <div class="info-lote">
                    <div>Lot: <span>{{ $query->lot }}</span></div>
                    <div>Initial Value: <span>R${{ $query->initial_value }}</span></div>
                    <div>Start: <span>{{ $query->start }}</span></div>
                    <div>End: <span>{{ $query->end }}</span></div>
                </div>
            </div>

            <div class="col-sm">
                <div class="lance" style="background-color: #eeeeee">
                    <div style="font: italic; background-color:rgba(6, 33, 92, 0.842); color: white" align="center">Aberto para lance</div>
                    <div align="center">Last Bid</div>
                    <div align="center">
                        <span style="font-size:x-large ">
                            @if ($query->final_value == '')
                            R${{ $query->initial_value }}
                            @else
                            R${{ $query->final_value }}
                            @endif
                        </span>
                    </div>
                </div>
                <br>

                <div class="lance" style="background-color: #eeeeee">
                    <div style="font: italic; background-color:rgba(6, 33, 92, 0.842); color: white" align="center">Historic Bids</div>
                    <table class="table table-bordered table-sm" style="font-size: x-small">
                        <thead >
                            <tr>
                              <th scope="col">Value</th>
                              <th scope="col">User</th>
                              <th scope="col">Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($bids as $bid)
                            <tr>
                                <th>R$ {{ number_format($bid->value, 2, ",",".") }}</th>
                                <th>{{ $bid->user_name }}</th>
                                <th>{{ date("d-m-Y h:i", strtotime($bid->bid_date)) }}</th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>                            
                </div>
                

                <div>
                    <form method="post" action="{{ route('lot.bid.store') }}">
                        @csrf
                        <input type="hidden" id="lot_id" name="lot_id" value="{{ $query->lot }}">
                        <input type="hidden" id="user_id" name="user_id" value="{{ auth()->user()->id }}">
                        <input type="hidden" id="final_value" name="final_value" value="{{ $query->final_value }}">
                        <div class="form-group">
                            <label for="value">Value</label>
                            <input type="text" class="form-control money" id="value" name="value" required>
                            @if ($errors->any())
                            <div class="invalid-feedback">
                                @foreach ($errors->all() as $error)
                                {{ $error }}
                                @endforeach
                            </div>
                            @endif
                        </div>


                        <button type="submit" class="botao-lance">Bid!</button>
                    </form>
                </div>
            </div>

            <div class="col-sm">
                <div class="contagem">
                    <div>
                        <span class="numero" id="dia"></span><span class="numero" id="hora"></span><span class="numero" id="minuto"></span><span class="numero" id="segundo"></span> 
                    </div>
                </div>
            </div>
        </div>
        <br>
        <h4>Descrição Lote</h4>
        <div class="descricao-lote">
            {{ $query->description }}
        </div>
    </div>
    
    @endif
    @endforeach
</div>

@extends('lot.functions.app', ['query' => $query])

@endsection
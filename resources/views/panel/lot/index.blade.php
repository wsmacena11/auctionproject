<div class="container" style="padding-top:10px;">
    <form action="{{ route('lot.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <h3>Cadastro de Lote</h3>
        <div class="form-row">
            <div class="col-2">
                <label for="start">Data Início</label>
                <input name="start" id="start" type="text" class="date form-control" placeholder="">
            </div>
            <div class="col-2">
                <label for="end">Data Fim</label>
                <input name="end" id="end" type="text" class="date form-control" placeholder="">
            </div>
            <div class="col-2">
                <label for="initial_value">Valor Inicial</label>
                <input name="initial_value" type="text" class="money form-control" placeholder="" step="0.01" min="0.01">
            </div>

            <div class="col-2">

                <label for="book_id">Book</label>
                <select name= "book_id" class="form-control" id="book_id">
                    <option value="0" selected disabled></option>
                    @foreach ($book as $books)
                    <span>$book->title</span>
                        <option value='{{ $books->id }}'>{{ $books->title }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-3">
                <label for="image">Imagem</label>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" name="image">
                    <label class="custom-file-label" for="image">Choose file</label>
                </div>
            </div>


        </div>

        <div class="form-row">

            <div class="col-6">
                <label for="description">Descrição</label>
                <textarea id="description" name="description" type="text" class="form-control" rows="3" maxlength="255"> </textarea>
            </div>

            <div class="col-2">

                <label for="status_id">Status</label>
                <select name= "status_id" class="form-control" id="status_id">
                    <option value="0" selected disabled></option>

                    <h1>$status->description</span>
                    @foreach ($status as $statuses)
                        <option value='{{ $statuses->id }}'>{{ $statuses->description }}</option>
                    @endforeach
                </select>
            </div>

        </div>

        <div class="form-row">

            <div class="col-3">
                <br>
                <button type="submit" class="btn btn-success">Cadastrar</button>
            </div>
        </div>
    </form>
    <br>
</div>

<div class="container">
    <h1>Consulta de Lotes</h1>
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th>Lot</th>
                <th>Book</th>
                <th>Start</th>
                <th>End</th>
            </tr>
        </thead>
        <tbody>


            @foreach ($lotbook as $lots)
                <tr>
                    <th>{{ $lots->lot }}</th>
                    <th>{{ $lots->title }}</th>
                    <th>{{ $lots->start }}</th>
                    <th>{{ $lots->end  }}</th>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="container" style="padding-top:10px;">
    <form action="{{ route('category.create') }}" method="POST">
        @csrf
        <h3>Category</h3>
        <div class="form-row">
            <div class="col-3">
                <label for="name">Category name</label>
                <input name="name" type="text" id="name" class="form-control" placeholder="">
            </div>
        </div>

        <div class="form-row">
            <div class="col-3">
                <br>
                <button type="submit" class="btn btn-success">Add</button>
            </div>
        </div>

    </form>
    <br>
</div>

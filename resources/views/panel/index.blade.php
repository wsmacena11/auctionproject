@extends('layouts.app')

@include('layouts._includes.nav')

@section('content')
    <div class="container sombra">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="profile-tab" data-toggle="tab" href="#category" role="tab" aria-controls="profile" aria-selected="false">Categories</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="lot-tab" data-toggle="tab" href="#lot" role="tab" aria-controls="contact" aria-selected="false">Lots</a>
            </li>
          </ul>
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
              <br><br>
              <h2>User Page</h2>
              <br>
            </div>
            <div class="tab-pane fade" id="category" role="tabpanel" aria-labelledby="profile-tab">
                @include('panel.status.index')
            </div>
            <div class="tab-pane fade" id="lot" role="tabpanel" aria-labelledby="contact-tab">
              @include('panel.lot.index')
            </div>
          </div>
    </div>
@endsection







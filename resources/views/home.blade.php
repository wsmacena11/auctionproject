@extends('layouts.app')

@include('layouts._includes.nav')

@section('content')
<div class="container mid">

    <h2 style="text-align: center">Lotes Recentes</h2>
    @foreach ($lotbook->chunk(3) as $lots)
        <div class="row">
            @foreach ($lots as $query)
            <div class="col-sm-4">
                <div class="card">
                    <img class="card-img-top" src="{{ asset('img/lots/' . $query->image) }}" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">Lote Nº: {{ $query->lot }}</h5>
                        <p class=" card-text-right">{{ $query->description }}</p>
                        
                        <div>
                            <a href="{{ route('lot.bid', $query->lot) }}" class="btn btn-primary">Detalhes</a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <br>
    @endforeach
    
    {{-- <div class="row">
        <div class="col-sm-4">
            <div class="card">
                <h5 class="card-title" style="text-align: center">Lote 1</h5>
                <img src="..." class="card-img-top" alt="...">
                <div class="card-body">
                    
                    <h5 class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</h5>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card">
                <h5 class="card-title" style="text-align: center">Lote 2</h5>
                <img src="..." class="card-img-top" alt="...">
                <div class="card-body">
                    
                    <h5 class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</h5>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card">
                <h5 class="card-title" style="text-align: center">Lote 3</h5>
                <img src="..." class="card-img-top" alt="...">
                <div class="card-body">
                    
                    <h5 class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</h5>
                </div>
            </div>
        </div>

    </div> --}}
    
    

    @guest
    @else
        <div class="menu-destaque">
            <ul>
                <li>
                    <a href="{{ asset(route('lot.index')) }}">Lots</a>
                </li>
            </ul>
        </div>     
    @endguest
    


    <br>
    @include('layouts._includes._carousel')

    <h1 class="titulo" style="text-align: center">#Thaly Cake Factory</h1>
    <hr>

    <section class="noticias">
        <div class="row noticias-card">
            <div class="col-s-12.col-l-9">
                <img src="{{ asset('img/noticias/livrovelho.jpg') }}">
                <h3>5 livros perdidos que você nunca poderá ler!</h3>
            </div>
            <div class="col-s-12.col-l-3">
                Conheça 5 livros que, embora tivessem potencial para se tornarem clássicos, nunca chegaram a ser publicados. São livros, rascunhos e manuscritos perdidos ao longo dos tempos
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-s-12.col-l-9">
                <a href="https://veja.abril.com.br/entretenimento/bienal-do-livro-de-sao-paulo-e-cancelada-por-causa-da-pandemia/">
                    <img src="https://veja.abril.com.br/wp-content/uploads/2018/08/entretenimento-bienal-do-livro-20120711-002.jpg?quality=70&strip=info&w=680&h=453&crop=1">
                    <h3>Bienal do Livro de São Paulo é cancelada por causa da pandemia</h3>
                </a>
                
            </div>
            <div class="col-s-12.col-l-3">
                info direita etc loren ipsun pbla bla bla std eh nois
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-s-12.col-l-9">
                <img src="https://veja.abril.com.br/wp-content/uploads/2020/06/design-sem-nome-5.png?w=680&h=453&crop=1">
                <h2>Menino recebe em sua casa, livro com...</h2>
            </div>
            <div class="col-s-12.col-l-3">
                
            </div>
        </div>
        <hr>
        
    </section>


</div>
@endsection
@section('footer')
<footer>
    <div class="principal container">
        <div class="links">
            <ul>
                <li>Bienal</li>
                <li>CCBB</li>
                <li>Museu do Amanhã</li>
            </ul>
        </div>
    </div>
</footer>
@endsection
<div class="justify-content-md-center">
    <div id="carouselExampleControls" class="carousel slide carousel-fade" data-ride="carousel" >
        <div class="carousel-inner" >
            <div class="carousel-item active">
                <img class="d-block w-100 img-fluid" src="{{ asset('img/book1.jpg') }}" alt="slide">
            </div>
            <div class="carousel-item" >
                <img class="d-block w-100 responsive img-fluid" src="{{ asset('img/book2.jpg') }}" alt="slide2">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100 img-fluid" src="{{ asset('img/book3.jpg') }}" alt="slide3">
            </div>
        </div>
    
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
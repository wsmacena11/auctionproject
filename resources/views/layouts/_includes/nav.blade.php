<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{ asset('img/b8.png') }}" width="40" height="40" alt="">
            {{ config('app.name', 'Laravel') }}
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link active" href="{{ route('lot.index') }}">Lots <span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link" href="#">Contato</a>
            </div>

            {{-- Login, Cadastrar-se --}}
            <div class="navbar-nav ml-auto">
                @guest
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="{{ route('login') }}">{{ __('Login') }}</a>
                        <div class="dropdown-menu">
                            <form method="POST" action="{{ route('login') }}" class="px-4 py-3">
                                @csrf
                                <div class="form-group">
                                    <label for="email">E-mail</label>
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                    {{-- <input type="email" class="form-control" id="email"> --}}
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="password">Senha</label>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                    {{-- <input type="password" class="form-control" id="password"> --}}
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    {{-- <input type="checkbox" class="form-check-input" id="checkauth"> --}}

                                    <label class="form-check-label" for="checkauth">
                                        {{ __('Lembrar') }}
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-primary">{{ __('Logar') }}</button>
                            </form>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('register') }}">Novo por aqui? Cadastre-se!</a>
                            <a class="dropdown-item" href="{{ route('password.request') }}">Esqueceu a Senha?</a>
                        </div>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->firstname }} {{ Auth::user()->lastname }} <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('book.index') }}">Books</a>
                            <a class="dropdown-item" href="{{ route('panel.index') }}">User Panel</a>
                            <a class="dropdown-item" href="{{ route('testes.index') }}">Tests</a>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </div>
            {{-- Fim Login, Cadastrar-se --}}


        </div>
    </div>

</nav>
